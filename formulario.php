<?php
include_once 'conexion.php';
$bd = new Conexion();

if (isset($_GET['extra'])) {
	$extra = $_GET['extra'];
	?>
	<html>
	<head>
		<title>Extra</title>
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="card shadow-sm mt-2">
				<form action="formulario.php?extra=<?=$extra?>" method="POST" id="formulario">
					<input type="hidden" name="extra" value="<?=$_GET['extra']?>">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-6 form-group">
								<label class="fw-bold">Nombre completo</label>
								<input type="text" name="nombre" required="" class="form-control" id="nombre">
							</div>
							<div class="col-lg-6 form-group">
								<label class="fw-bold">Curso</label>
								<select name="curso" class="form-control" required="" id="curso">
									<option value="" selected="">Seleccione una opcion...</option>
									<?php if ($extra == 1) {
										?>
										<option value="2A">2A</option>
										<option value="2B">2B</option>
										<option value="3A">3A</option>
										<option value="3B">3B</option>
										<?php
									}
									if ($extra == 2) {
										?>
										<option value="4A">4A</option>
										<option value="4B">4B</option>
										<option value="5A">5A</option>
										<option value="5B">5B</option>
										<?php
									}

									if ($extra > 2) {
										?>
										<option value="2A">2A</option>
										<option value="2B">2B</option>
										<option value="3A">3A</option>
										<option value="3B">3B</option>
										<option value="4A">4A</option>
										<option value="4B">4B</option>
										<option value="5A">5A</option>
										<option value="5B">5B</option>
										<?php
									}
									?>
								</select>
							</div>
							<div class="col-lg-12 form-group mt-4">
								<a href="index.php" class="btn btn-danger btn-sm" value="<?=$_GET['extra']?>">
									Cancelar
								</a>
								<button class="btn btn-success btn-sm formulario_enviar" type="button" value="<?=$extra?>">
									Enviar
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<div id="alerta">
		</div>
	</body>
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="funciones.js"></script>
	</html>
	<?php

	if (isset($_POST['extra'])) {
		$nombre = $_POST['nombre'];
		$curso  = $_POST['curso'];
		$extra  = $_POST['extra'];

		$insertar = $bd->query("INSERT INTO registro (nombre, curso, extra) VALUES ('" . $nombre . "', '" . $curso . "', '" . $extra . "')");
		if ($insertar == true) {
			?>
			<div class="alert alert-success mt-4 text-center" style="width:80%; margin-left:10%;" role="alert">
				Se ha guardado correctamente
			</div>
			<script>
				setInterval(function() {
					window.location.replace('index.php');
				}, 900);
			</script>
			<?php
		}
	}

}