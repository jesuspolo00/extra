$(document).ready(function() {
    //var extra_id = $(".enlace").attr('value');
    $(".formulario_enviar").click(function(e) {
        e.preventDefault();
        var extra = $(this).attr('value');
        var nombre = $("#nombre").val();
        var curso = $("#curso").val().trim();
        if (nombre == "" && curso == "") {
            $("#nombre").focus();
            $("#alerta").html('<div class="alert alert-danger mt-4 text-center" style="width:80%; margin-left:10%;" role="alert">' + 'Campos vacios' + '</div>')
        }
        if (nombre != "" && curso == "") {
            $("#curso").focus();
            $("#alerta").html('<div class="alert alert-danger mt-4 text-center" style="width:80%; margin-left:10%;" role="alert">' + 'Campos vacios' + '</div>')
        }
        if (nombre == "" && curso != "") {
            $("#nombre").focus();
            $("#alerta").html('<div class="alert alert-danger mt-4 text-center" style="width:80%; margin-left:10%;" role="alert">' + 'Campos vacios' + '</div>')
        }
        if (nombre != "" && curso != "") {
            //$("#alerta").html('<div class="alert alert-danger mt-4 text-center" style="width:80%; margin-left:10%;" role="alert">' + 'Campos vacios' + '</div>')
            descontarCupo(extra);
        }
    });
    /*------------------------*/
    /*    $(".enlace_cancelar").click(function(e) {
            e.preventDefault();
            var extra = $(this).attr('value');
            sumarCupo(extra);
        });*/
    /*------------------------*/
    function descontarCupo(extra) {
        $.ajax({
            url: "descontar.php",
            method: "POST",
            data: {
                extra: extra
            },
            dataType: "json",
            success: function(data) {
                $("#formulario").submit();
            }
        });
    }

    function sumarCupo(extra) {
        $.ajax({
            url: "sumar.php",
            method: "POST",
            data: {
                extra: extra
            },
            dataType: "json",
            success: function(data) {
                //contarCupo(extra)
                window.location.replace('index.php');
            }
        });
    }

    function contarCupo(view = '') {
        $.ajax({
            url: "contar.php",
            method: "POST",
            data: {
                view: view
            },
            dataType: "json",
            success: function(data) {
                var array = data.extras;
                array.forEach(function(datos) {
                    $("#" + datos.extra).text(datos.cupos);
                    if (datos.cupos <= 0) {
                        $("#" + datos.extra).hide();
                        $("#enlace" + datos.extra).hide();
                    } else {
                        $("#" + datos.extra).show();
                        $("#enlace" + datos.extra).show();
                    }
                });
            }
        });
    }
    contarCupo();
    setInterval(function() {
        contarCupo();;
    }, 900);
});