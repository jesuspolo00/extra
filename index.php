<?php
include_once 'conexion.php';
$bd = new Conexion();
?>
<html>
<head>
	<title>Extra</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container-fluid">
		<div class="row p-4">
			<div class="col-lg-3"  id="enlace1">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=1" class="enlace" value="1">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="1">
							0
						</span>
						<div class="card-body">
							<img src="img/futbol_m1.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Futbol Masculino 2-3</h5> -->
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-3"  id="enlace2">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=2" class="enlace" value="2">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="2">
							0
						</span>
						<div class="card-body">
							<img src="img/futbol_m2.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Futbol Masculino 4-5</h5> -->
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-3"  id="enlace3">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=3" class="enlace" value="3">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="3">
							0
						</span>
						<div class="card-body">
							<img src="img/futbol_f.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Futbol Femenino</h5> -->
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-3"  id="enlace4">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=4" class="enlace" value="4">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="4">
							0
						</span>
						<div class="card-body">
							<img src="img/baloncesto.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Baloncesto</h5> -->
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-3"  id="enlace5">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=5" class="enlace" value="5">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="5">
							0
						</span>
						<div class="card-body">
							<img src="img/danza.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Patinaje</h5> -->
						</div>
					</a>
				</div>
			</div>

			<div class="col-lg-3"  id="enlace6">
				<div class="card shadow-sm rounded mb-2">
					<a href="formulario.php?extra=6" class="enlace" value="6">
						<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger" id="6">
							0
						</span>
						<div class="card-body">
							<img src="img/voleibol.jpg" class="card-img-top" alt="">
							<!-- <h5 class="card-title text-center text-uppercase fw-bold">Voleibol</h5> -->
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="funciones.js"></script>
</html>
