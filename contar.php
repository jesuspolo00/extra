<?php
include_once 'conexion.php';
$bd = new Conexion();

$query = $bd->query("SELECT * FROM cupos");

$extra_id = [];
$cupo     = [];

while ($resultado = mysqli_fetch_array($query)) {
	$extra_id[] = array('extra' => $resultado['extra'], 'cupos' => $resultado['cupo']);
}

echo json_encode(['extras' => $extra_id]);
