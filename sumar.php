<?php
include_once 'conexion.php';
$bd = new Conexion();

$extra     = $_POST['extra'];
$query     = $bd->query("SELECT * FROM cupos WHERE extra = " . $extra);
$resultado = mysqli_fetch_row($query);

if ($resultado[2] < 20) {
	$descontar = ($resultado[2] + 1);
} else {
	$descontar = $resultado[2];
}

$actualizar = $bd->query("UPDATE cupos SET cupo = " . $descontar . " WHERE extra = " . $extra);

if ($actualizar == true) {
	$mensaje = 'ok';
} else {
	$mensaje = 'error';
}

echo json_encode($mensaje);
